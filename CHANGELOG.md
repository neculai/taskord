# CHANGELOG

- Sep 15, 2020 - **v0.0.2-alpha** - https://dev.to/taskord/taskord-changelog-sep-15-fb1
- Sep 9, 2020 - **v0.0.1-alpha** - First Alpha Release
